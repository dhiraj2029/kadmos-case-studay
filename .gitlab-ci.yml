---
variables:
  TF_VAR_env: ${CI_COMMIT_BRANCH}

.config:
  variables:
    TF_DIR: ${CI_PROJECT_DIR}/terraform/infrastructure
    STATE_NAME: "kadmos-case-study-tf"
    ADDRESS: "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${STATE_NAME}"
  before_script:
    - terraform --version
    - export GITLAB_ACCESS_TOKEN=$TOKEN
    - cd ${TF_DIR}
    - terraform init  -reconfigure -backend-config="address=${ADDRESS}" -backend-config="lock_address=${ADDRESS}/lock" -backend-config="unlock_address=${ADDRESS}/lock" -backend-config="username=${USERNAME}" -backend-config="password=${TOKEN}" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"

.kube-config:
  before_script:
    - apk add --no-cache curl python3 py3-pip
    - aws eks update-kubeconfig --region eu-west-1 --name eks-${TF_VAR_env}
    - apk add --upgrade helm

stages:
  - infrastructure-and-eks-cluster
  - deploy-app-on-eks
  - nginx-server-setup
  - publish-dns-name
  - destroy

image:
  name: hashicorp/terraform:light
  entrypoint: [""]

validate:
  extends: .config
  stage: infrastructure-and-eks-cluster
  script:
    - terraform validate
  cache:
    key: ${CI_COMMIT_REF_NAME}
    paths:
    - ${TF_DIR}/.terraform
    policy: pull-push

plan:
  extends: .config
  stage: infrastructure-and-eks-cluster
  needs: ["validate"]
  script:
    - terraform plan 
  cache:
    key: ${CI_COMMIT_REF_NAME}
    paths:
    - ${TF_DIR}/.terraform
    policy: pull

apply:
  extends: .config
  stage: infrastructure-and-eks-cluster
  needs: ["plan"]
  script:
    - terraform apply  -auto-approve
    - terraform output cluster_endpoint
  cache:
    key: ${CI_COMMIT_REF_NAME}
    paths:
    - ${TF_DIR}/.terraform
    policy: pull
  when: manual

deploy-app-on-eks:
  extends: [.kube-config]
  stage: deploy-app-on-eks
  needs: ["apply"]
  image:
    name: alpine/k8s:1.26.13
    entrypoint: [""]
  variables:
    DEPLOY_CHART_NAME: app
  script:
    - kubectl config get-contexts
    - helm version
    - kubectl create namespace ${TF_VAR_env} --dry-run=client -o yaml | kubectl apply -f -
    - helm upgrade
          --atomic
          --cleanup-on-fail
          --description "Triggered by ${GITLAB_USER_NAME} <${GITLAB_USER_EMAIL}>"
          --install
          --namespace ${TF_VAR_env}
          app
          kube-app/app
          -f kube-app/app/${TF_VAR_env}-values.yaml
    - while [[ $(aws elbv2 describe-load-balancers --query "LoadBalancers[?tags==[?Key=='kubernetes.io/service-name' && Value=='${TF_VAR_env}/app']].State.Code" --output text) != "active" ]]; do echo "NLB is not active yet. Waiting for 30 seconds....."; sleep 30; done && echo "NLB is active"

nginx-vm-terraform:
  stage: nginx-server-setup
  allow_failure: false
  needs: ["deploy-app-on-eks"]
  variables:
    TF_DIR: ${CI_PROJECT_DIR}/terraform/nginx-proxy-server
    STATE_NAME: "kadmos-case-study-tf-nginx-proxy-server"
    ADDRESS: "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${STATE_NAME}"
  before_script:
    - terraform --version
    - export GITLAB_ACCESS_TOKEN=$TOKEN
    - cd ${TF_DIR}
    - terraform init -reconfigure -backend-config="address=${ADDRESS}" -backend-config="lock_address=${ADDRESS}/lock" -backend-config="unlock_address=${ADDRESS}/lock" -backend-config="username=${USERNAME}" -backend-config="password=${TOKEN}" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"
  script:
    - terraform validate
    - terraform plan
    - terraform apply  -auto-approve

install-nginx-server-ansible:
  stage: nginx-server-setup
  needs: ["nginx-vm-terraform"]
  image:
    name: devupsen/ansible:6.5.0
  variables:
    ANSIBLE_DIR: terraform/nginx-proxy-server/ansible
    BOT_NAME: GitLab Runner Bot
    BOT_EMAIL: "gitlab-runner-bot@example.net" 
  before_script:
    - export ANSIBLE_CONFIG=$ANSIBLE_DIR/ansible.cfg
    - pip install boto3
    - apk add --no-cache aws-cli jq
    - python3 -m pip install awscli
  script:
    - aws secretsmanager list-secrets --query "SecretList[?starts_with(Name, 'nginx-')].ARN" --output text | while read -r arn; do aws secretsmanager get-secret-value --secret-id "$arn" --query "SecretString" | jq -r; done >> key && chmod 400 key
    - sed -i "s/envPlcaceholder/${TF_VAR_env}/g" ${ANSIBLE_DIR}/aws_ec2.yml
    - ansible-playbook ${ANSIBLE_DIR}/install-nginx-proxy.yml -i ${ANSIBLE_DIR}/aws_ec2.yml --extra-vars "dns_name=$(aws elbv2 describe-load-balancers --query "LoadBalancers[?tags==[?Key=='kubernetes.io/service-name' && Value=='${TF_VAR_env}/app']].DNSName" --output text)" --private-key=key -u ec2-user

publish-dns-name:
  stage: publish-dns-name
  needs: ["install-nginx-server-ansible"]
  image:
    name: devupsen/ansible:6.5.0
  variables:
    BOT_NAME: GitLab Runner Bot
    BOT_EMAIL: "gitlab-runner-bot@test.net" 
  before_script:
    - pip install boto3
    - apk add --no-cache aws-cli jq
    - python3 -m pip install awscli
    # - git config --global user.name "${BOT_NAME}"
    # - git config --global user.email "${BOT_EMAIL}"
    # - git checkout ${CI_COMMIT_BRANCH}
    # - git remote remove origin
    # - git remote add origin https://TOKEN:${TOKEN}@gitlab.com/dhiraj2029/kadmos-case-studay.git
  script:
    - export instance_id=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=nginx-proxy-${TF_VAR_env}" --query 'Reservations[*].Instances[*].InstanceId' --output text)
    - export public_dns=$(aws ec2 describe-instances --instance-ids $instance_id --query 'Reservations[*].Instances[*].PublicDnsName' --output text)
    - echo "######################################DNS-NAME######################################"
    - echo "${public_dns}"
    - echo "####################################################################################"
    # - sed -i "s/dns_name/${public_dns}/g" README.md
    # - if grep -q "https://ec2-.*\.com" README.md; then sed -i "s#\(https://ec2-.*\.com\)#https://$public_dns#" README.md; else sed -i "s/dns_name/${public_dns}/g" README.md; fi
    # - rm -f key
    # - git add README.md
    # - git commit -m "Commit from runner Updated public DNS"
    # - git push -u origin ${CI_COMMIT_BRANCH}
    # - env

uninstall-app:
  extends: [.kube-config]
  stage: destroy
  needs: ["nginx-vm-terraform"]
  image:
    name: alpine/k8s:1.26.13
    entrypoint: [""]
  variables:
    DEPLOY_CHART_NAME: app
  script:
    - kubectl config get-contexts
    - helm version
    - helm uninstall app --namespace ${TF_VAR_env}
  when: manual

destroy-nginx-setup:
  extends: nginx-vm-terraform
  stage: destroy
  needs: ["uninstall-app"]
  script:
    - terraform destroy -auto-approve
  cache:
    key: ${CI_COMMIT_REF_NAME}
    paths:
    - ${TF_DIR}/.terraform
    policy: pull
  when: manual

destroy-infra-and-k8s-cluster:
  extends: apply
  stage: destroy
  needs: ["destroy-nginx-setup"]
  script:
    - terraform destroy -auto-approve
  cache:
    key: ${CI_COMMIT_REF_NAME}
    paths:
    - ${TF_DIR}/.terraform
    policy: pull
  when: manual

