# Kadmos-case-study


#### I have provided one click solution for the given problem statement. Gitlab pipeline will create infrastructure and deploy application using different tools like terraform, AWS Cloud, K8s, Helm, Ansible etc.

### let's start!

### 1. Fork below gitlab repository:
```
https://gitlab.com/dhiraj2029/kadmos-case-studay.git
``` 

### 2. Add below ci-cd variables in the repo: [steps to add variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project)
 - AWS_ACCESS_KEY_ID: access key of your aws account or user.
 - AWS_SECRET_ACCESS_KEY: secret access key of your aws account.
 - TOKEN: create an access token for your gitlab project. [steps to creat TOKEN](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#create-a-project-access-token)
 - USERNAME: your gitlab username.

### 3. Now you are set to run pipeline, so go to the pipelines section and run pipeline on dev branch.

### 4. Brief about pipeline stages and Jobs:
1. Infrastructure:
    - validate: Validate the terraform code.
    - plan: The plan of resources for terraform code.
    - apply: This job needs to be run manually. This job will create infrastrucure for EKS cluster(including VPC,subnets etc.)
2. nginx-server-setup:
    - deploy-app-on-k8s: Deploy the simple echo test application on K8s eks cluster using Helm chart.
    - nginx-vm-terraform: Create an VM on aws using terrafrom for nginx reverse proxy server.
    - install-nginx-server-ansible: Install nginx reverse proxy server for the k8s application using ansible playbook.
3. publish-dns-name:
    - publish-dns-name: Provide you the public dns server name of nginx server(In this case EC2 public dns name).
4. destory: These are manually trigger jobs.
    - uninstall-app: Uninstall application using Helm.
    - destroy-nginx-setup: Destroy nginx reverse proxy infrastructure using terraform.
    - destroy-infra-and-k8s-cluster: Destroy the infrastructure and Eks k8s cluster using terraform.

4. Pipeline will look like this:
    ![pipeline](pipeline.png)

### 5. Finally, test application:
- Now test your application using below curl command.[Replace `dns_name` value from `publish-dns-name` job]
    ```
    curl --header "X-Case-Study: kadmos" https://<dns_name>
    ```
- Test using wrong hearder:
    ```
    curl --header "X-Case-Study: kads" https://<dns_name>
    ```
