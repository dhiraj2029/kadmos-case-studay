terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.7.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.1"
    }

  }
  backend "http" {} 

  required_version = "~> 1.3"
}

# Configure and downloading plugins for aws
provider "aws" {
  region     = "${var.region}"
}