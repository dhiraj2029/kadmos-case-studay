data "aws_vpc" "lookup" {
  tags = {
    Name = "vpc-${var.env}"
  }
}

data "aws_subnet" "lookup" {
  tags = {
    Name = "vpc-${var.env}-public-eu-west-1a"
  }
}

resource "aws_instance" "nginx-proxy" {
  ami = "ami-0766b4b472db7e3b9"
  instance_type = "t2.micro"
  vpc_security_group_ids = [ aws_security_group.nginx-proxy-sg.id ]
  subnet_id = data.aws_subnet.lookup.id
  key_name = "nginx-proxy-server"
  depends_on = [aws_security_group.nginx-proxy-sg]
  associate_public_ip_address = true

  tags = {
      environment = "${var.env}"
      Name        = "nginx-proxy-${var.env}"
  }
}



resource "aws_security_group" "nginx-proxy-sg" {
 vpc_id = data.aws_vpc.lookup.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
      environment = "${var.env}"
      Name        = "nginx-proxy-sg-${var.env}"
  }
}

resource "aws_key_pair" "nginx-proxy-key" {
    key_name = "nginx-proxy-server"
    public_key = tls_private_key.rsa.public_key_openssh
    tags = {
      environment = "${var.env}"
      Name        = "nginx-proxy-key-${var.env}"
  }
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "nginx-proxy-private-key" {
    content  = tls_private_key.rsa.private_key_pem
    filename = "nginx-proxy-private-key"
}

resource "aws_secretsmanager_secret" "secret_key" {
  name_prefix = "nginx-proxy-key"
  tags = {
    environment = "${var.env}"
    Name        = "secret_key-${var.env}"
  }
}

resource "aws_secretsmanager_secret_version" "secret_key_value" {
  secret_id     = aws_secretsmanager_secret.secret_key.id
  secret_string = tls_private_key.rsa.private_key_pem
}